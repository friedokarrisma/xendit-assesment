let MembersModel = require('../models/members.model')
let express = require('express')
let router = express.Router()

// Create a new members
router.post('/orgs/xendit/members', (req, res) => {
  if(!req.body) {
    return res.status(400).send('Request body is missing')
  }

  if(!req.body.email) {
    // ...
  }

  let model = new MembersModel(req.body)
  model.save().then(doc => {
      if(!doc || doc.length === 0) {
        return res.status(500).send(doc)
      }
      res.status(201).send(doc)
    }).catch(err => {
      res.status(500).json(err)
    })
})

// GET
router.get('/orgs/xendit/member', (req, res) => {
  if(!req.query.email) {
    return res.status(400).send('Missing URL parameter: email')
  }

  MembersModel.findOne({
    email: req.query.email
  }).then(doc => {
      res.json(doc)
    }).catch(err => {
      res.status(500).json(err)
    })
})

router.get('/orgs/xendit/members', (req, res) => {
  MembersModel.find()
  .sort({_id: -1})
  .then(doc => {
    res.json(doc)
  }).catch(err => {
    res.status(500).json(err)
  })
})

// UPDATE
router.put('/orgs/xendit/members', (req, res) => {
  if(!req.query.email) {
    return res.status(400).send('Missing URL parameter: email')
  }

  MembersModel.findOneAndUpdate({
    email: req.query.email
  }, req.body, {
    new: true
  }).then(doc => {
      res.json(doc)
    }).catch(err => {
      res.status(500).json(err)
    })
})

// DELETE
router.delete('/orgs/xendit/members', (req, res) => {
  if(!req.query.email) {
    return res.status(400).send('Missing URL parameter: email')
  }

  MembersModel.findOneAndRemove({
    email: req.query.email
  }).then(doc => {
      res.json(doc)
    }).catch(err => {
      res.status(500).json(err)
    })
})

module.exports = router