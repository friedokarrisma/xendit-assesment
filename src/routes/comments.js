let CommentsModel = require('../models/comments.model')
let express = require('express')
let router = express.Router()

// Create a new comments
router.post('/orgs/xendit/comments', (req, res) => {
  if(!req.body) {
    return res.status(400).send('Request body is missing')
  }

  if(!req.body.email) {
    // ...
  }

  let model = new CommentsModel(req.body)
  model.save().then(doc => {
      if(!doc || doc.length === 0) {
        return res.status(500).send(doc)
      }
      res.status(201).send(doc)
    }).catch(err => {
      res.status(500).json(err)
    })
})

// get all comments
router.get('/orgs/xendit/comments', (req, res) => {
  CommentsModel.find()
  .sort({_id: -1})
  .where('flag').equals('0')
  .then(doc => {
    res.json(doc)
  }).catch(err => {
    res.status(500).json(err)
  })
})

// DELETE
router.put('/orgs/xendit/comments', (req, res) => {
  if(!req.query.email) {
    return res.status(400).send('Missing URL parameter: email')
  }

  CommentsModel.findOneAndUpdate({
    email: req.query.email
  }, req.body, {
    new: true
  }).then(doc => {
      res.json(doc)
    }).catch(err => {
      res.status(500).json(err)
    })
})

module.exports = router