let mongoose = require('mongoose')
mongoose.connect(`mongodb+srv://kanebokering:Uayop7NFioGVEZet@cluster0-w3hbb.gcp.mongodb.net/test?retryWrites=true&w=majority`, {'useNewUrlParser': true})
mongoose.set('useCreateIndex', true);

let MembersSchema = new mongoose.Schema({
  name: String,
  email: {
    type: String,
    required: true,
    unique: true
  },
  avatarUrl: String,
  followers: String,
  following: String
})

module.exports = mongoose.model('Members', MembersSchema)