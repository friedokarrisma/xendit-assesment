let mongoose = require('mongoose')
mongoose.connect(`mongodb+srv://kanebokering:Uayop7NFioGVEZet@cluster0-w3hbb.gcp.mongodb.net/test?retryWrites=true&w=majority`, {'useNewUrlParser': true})
mongoose.set('useCreateIndex', true);

let CommentsSchema = new mongoose.Schema({
  comment: {
    type: String,
    required: true
  },
  flag: {
    type:  String,
    default: '0'
  },
  email: {
    type: String,
    required: true,
    unique: true
  }
})

module.exports = mongoose.model('Comments', CommentsSchema)